## Overview ##

**pspvg** is a 3D virtual globe homebrew application for Sony PSP.

It supports multiple planets/layers and 3D terrain, images are downloaded on demand via WiFi connection.

[Download latest binary release](http://code.mifki.com/pspvg/downloads/PSPVirtualGlobe-0.53.zip)