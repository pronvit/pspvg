#include "types.h"

typedef struct
{
    unsigned char pixels[256*256*4];
    Vertex vs[24*11+18*3] __attribute__((aligned(16)));
    Vertex vsf[24*11+18*3] __attribute__((aligned(16)));
    
    bool free;
} TileMem;

void memmgr_init();
TileMem* memmgr_get();
void memmgr_release(TileMem *mem);
