#include <stdio.h>
#include <string.h>

#include "types.h"
#include "index.h"

FILE *indf;
FILE *imgf;
INDEX_DATA eind;

extern LAYER *curlayer;

void index_open()
{
    memset(&eind, 0, sizeof(INDEX_DATA));

    char path[100];
    sprintf (path, "Data/%s/ind", curlayer->dir);
    indf = fopen(path, "r+b");
    if (!indf)
    {
        indf = fopen(path, "w+b");
        fwrite(INDEX_HDR_IND, sizeof(INDEX_HDR_IND), 1, indf);
    
        for (int i = 0; i < 16; i++)
            fwrite(&eind, sizeof(INDEX_DATA), 1, indf);
        
        fflush(indf);
    }

    sprintf (path, "Data/%s/img", curlayer->dir);
    imgf = fopen(path, "r+b");
    if (!imgf)
    {
        imgf = fopen(path, "w+b");
        fwrite(INDEX_HDR_IMG, sizeof(INDEX_HDR_IMG), 1, imgf);
        fflush(imgf);
    }
}

void index_close()
{
    fclose (indf);
    fclose (imgf);
}

void index_read(Tile *t)
{
    fseek(indf, t->indo, SEEK_SET);
    fread(&t->ind, sizeof(INDEX_DATA), 1, indf);
    
    if (!t->ind.indoffset[0])
    {
        fseek(indf, 0, SEEK_END);
        for (int i = 0; i < 4; i++)
        {
            t->ind.indoffset[i] = ftell(indf);
            fwrite(&eind, sizeof(INDEX_DATA), 1, indf);
        }
        
        index_write(t);
    }
}

void index_write(Tile *t)
{
    fseek(indf, t->indo, SEEK_SET);
    fwrite(&t->ind, sizeof(INDEX_DATA), 1, indf);
    fflush(indf);
}
