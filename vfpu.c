#include "vfpu.h"

inline float x_fabsf(float x) {
	float r;
	__asm__ volatile( "abs.s %0, %1" : "=f"(r) :"f"(x) : "memory");
	return r;
}

inline float x_sinf(float x) { 
   float result; 
   __asm__ volatile (
      "mtv %1, S000\n"
      "vcst.s S001, VFPU_2_PI\n"
      "vmul.s S000, S000, S001\n"
      "vsin.s S000, S000\n"
      "mfv %0, S000\n"
      : "=r"(result)
      : "r"(x)
   );
   return result;
}

inline float x_cosf(float x)	{
   float result;
   __asm__ volatile (
      "mtv %1, S000\n"
      "vcst.s S001, VFPU_2_PI\n"
      "vmul.s S000, S000, S001\n"
      "vcos.s S000, S000\n"
      "mfv %0, S000\n"
      : "=r"(result)
      : "r"(x)
   );
   return result;
}

inline void x_sincosf(float r, float *s, float *c) {
	__asm__ volatile (
		"mtv      %2, S002\n"
		"vcst.s   S003, VFPU_2_PI\n"
		"vmul.s   S002, S002, S003\n"
		"vrot.p   C000, S002, [s, c]\n"
		"mfv      %0, S000\n"
		"mfv      %1, S001\n"
	: "=r"(*s), "=r"(*c): "r"(r));
}

inline float x_tanf(float x)	{
   float result;
   __asm__ volatile (
      "mtv %1, S000\n"
      "vcst.s S001, VFPU_2_PI\n"
      "vmul.s S000, S000, S001\n"

      "vrot.p C002, S000, [s, c]\n" //sin -> $2, cos -> $3

      "vrcp.s  S003, S003\n" //1/cos -> $2
      "vmul.s  S000, S002, S003\n" //sin(x) * 1/cos(x) = tan(x) -> $0

      "mfv %0, S000\n"
      : "=r"(result)
      : "r"(x)
   );
   return result;
}


inline float x_acosf(float x)
{
    float result;
    __asm__ volatile (
        "mtv     %1, S000\n"
        "vcst.s  S001, VFPU_PI_2\n"
        "vasin.s S000, S000\n"
        "vocp.s  S000, S000\n"
        "vmul.s  S000, S000, S001\n"
        "mfv     %0, S000\n"
        : "=r"(result) : "r"(x));
    return result;
}

inline float x_asinf(float x)
{
    float result;
    __asm__ volatile (
        "mtv     %1, S000\n"
        "vcst.s  S001, VFPU_PI_2\n"
        "vasin.s S000, S000\n"
        "vmul.s  S000, S000, S001\n"
        "mfv     %0, S000\n"
        : "=r"(result) : "r"(x));
    return result;
}

inline float x_atanf(float x)
{
    float result; 
    __asm__ volatile ( //atan(x) = asin(x/sqrt(x*x+1))
		"mtv      %1, S000\n"
		"vmul.s   S001, S000, S000\n"
		"vadd.s   S001, S001, S001[1]\n"
		"vrsq.s   S001, S001\n"
		"vmul.s   S000, S000, S001\n"
		"vasin.s  S000, S000\n"
		"vcst.s   S001, VFPU_PI_2\n"
		"vmul.s   S000, S000, S001\n"
		"mfv      %0, S000\n"
        : "=r"(result) : "r"(x));
    return result;
}

inline float x_atan2f(float y, float x) {
	float r;

	if (x_fabsf(x) >= x_fabsf(y)) {
		r = x_atanf(y/x);
		if   (x < 0.0f) r += (y>=0.0f ? PI : -PI);
	} else {
		r = -x_atanf(x/y);
		r += (y < 0.0f ? -PIOVER2 : PIOVER2);
	}
	return r;
}


inline float x_expf(float x) {
    float result;
    __asm__ volatile (
        "mtv     %1, S000\n"
        "vcst.s  S001, VFPU_LN2\n"
        
        "vrcp.s  S001, S001\n"
        "vmul.s  S000, S000, S001\n"
        
        "vexp2.s S000, S000\n"
        "mfv     %0, S000\n"
        : "=r"(result) : "r"(x));
    return result;
}

inline float x_logf(float x) {
    float result;
    __asm__ volatile (
        "mtv     %1, S000\n"
        "vcst.s  S001, VFPU_LOG2E\n"
        "vrcp.s  S001, S001\n"
        "vlog2.s S000, S000\n"
        "vmul.s  S000, S000, S001\n"
        "mfv     %0, S000\n"
        : "=r"(result) : "r"(x));
    return result;
}


inline float x_sqrtf(float x) { 
   float result; 
   __asm__ volatile (
      "mtv %1, S000\n"
      "vsqrt.s S000, S000\n"
      "mfv %0, S000\n"
      : "=r"(result)
      : "r"(x)
   );
   return result;
}


inline float x_fmodf(float x, float y) {
	float result;
	// return x-y*((int)(x/y));
	__asm__ volatile (
		"mtv       %2, S001\n"
		"mtv       %1, S000\n"
		"vrcp.s    S002, S001\n"
		"vmul.s    S003, S000, S002\n"
		"vf2iz.s   S002, S003, 0\n"
		"vi2f.s    S003, S002, 0\n"
		"vmul.s    S003, S003, S001\n"
		"vsub.s    S000, S000, S003\n"
		"mfv       %0, S000\n"
	: "=r"(result) : "r"(x), "r"(y));
	return result;
}
