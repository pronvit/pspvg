#ifndef _H_TYPES
#define _H_TYPES

typedef struct {
	float u, v;
	unsigned int color;
//	float nx, ny, nz;
	float x, y, z;
} Vertex;

typedef struct {
	unsigned int color;
	float x, y, z;
} VertexCP;

#define URL_XYZ     1
#define URL_QKEY    2
#define URL_YAHOO   3
#define URL_OSM     4
#define URL_GMOON   5
#define URL_GMARS   6

#define IMG_JPG     1
#define IMG_PNG     2

typedef struct {
    const char *name;
    const char *dir;

    const char *url;
    const char *copystr;

    char urltype;
    char imgtype;
    
    bool z;
    bool usesrtm;
    char world; //0 - earth, 1 - moon, 2 - mars
} LAYER;

typedef struct
{
    long imgoffset;
    long indoffset[4];
} INDEX_DATA;

typedef struct
{
    char layer;
    char vexagg;
    
    char reserved[50];
} CONFIG;

typedef struct {
    char *title;

    float lat, lon, zm, tilt, azimuth;
} BOOKMARK;

#endif
