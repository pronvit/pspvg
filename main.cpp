#include <malloc.h>
#include <pspkernel.h>
#include <pspdisplay.h>
#include <pspdebug.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspctrl.h>
#include <string.h>
#include <pspkerneltypes.h>
#include <psphttp.h>
#include <pspsdk.h>
#include <pspvfpu.h>
#include <pspnet.h>
#include <pspnet_inet.h>
#include <pspnet_resolver.h>
#include <pspnet_apctl.h>
#include <psputility.h>
#include <psputility_netmodules.h>
#include <psputility_htmlviewer.h>
#include <stdarg.h>
#include <sys/unistd.h>
#include <intraFont.h>

#include "main.h"
#include "menu.h"	
#include "vram.h"
#include "CTimer.h"
#include "tile.h"
#include "vfpu.h"
#include "index.h"

PSP_MODULE_INFO("Virtual Globe", PSP_MODULE_USER, 0, 3);
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);
//PSP_HEAP_SIZE_KB(-1024*7);
PSP_HEAP_SIZE_KB(-1024*1);

#define BROWSER_MEMORY (4*1024*1024) 

static char list[1024*512] __attribute__((aligned(64)));

int dlsize = 0;
int lcnt = 0;

bool flat;

bool needupdate;

void *fbp0;
int fload, fmem;

u32 tickResolution;

int updcnt, dlcnt;
int listsize;

CONFIG config;

int mem;

SceUID dldruid;
SceUID dlsema;

float xrot;
float yrot;
float trot = 0.0f;
float krot = 0.0f;
float zm = 2000, zfv = 45.0 / 75.0;
float R = 1000;

float targetvcx = 0, targetvcy = 0, targetzm = zm;

float vcx, vcy, vlx, vly, vrx, vry;

Tile *dltile, *dltile2;

int fdlcnt;

#define VEXAGGERATION_BASE R/6378137.0f
#define MOVESPEED_NORMAL 0.1f
#define MOVESPEED_FLY 0.02f

float vexaggeration;

float ef;

int nucnt;

int dldr2(SceSize args, void *argp) {
    int l, scode;

    dlsema = sceKernelCreateSema("dlmutex", 0, 0, 1, 0);

    int templid = sceHttpCreateTemplate("Mozilla", 1, 0);
    //sceHttpEnableKeepAlive (templid);

    //int connid = sceHttpCreateConnectionWithURL(templid, "http://h1.ortho.tiles.virtualearth.net", 0); //maybe need new connection every time or when it drops???
    
    while (!exiting)
    {
        //sceKernelWaitSema(dlsema, 1, 0);
        if (dltile)
        {
            int connid = sceHttpCreateConnectionWithURL(templid, dltile->url, 0);
            int reqid = sceHttpCreateRequestWithURL(connid, PSP_HTTP_METHOD_GET, dltile->url, 0);
            sceHttpSendRequest(reqid, NULL, 0);
            sceHttpGetStatusCode(reqid, &scode);

            if (scode == 200)
            {
                dltile->dlsize = 0;
                while (dltile->mem && (l = sceHttpReadData(reqid, dltile->mem->pixels+dltile->dlsize, 1024*32)) > 0)
                    dltile->dlsize += l;
                fdlcnt++;
            } else
                dltile->bad = true;
                
            if (dltile->dlsize == 4768 && dltile->mem->pixels[0x300] == 0xAC) //img not avail. for ymsat layer
            {
                dltile->noimg = true;
                dltile->freeTile();
            }

            dltile->dl = false;
            dltile->dld = true;
            dltile->nonlocal = false;
            
            sceHttpDeleteRequest(reqid);
            sceHttpDeleteConnection(connid);
            
            dltile = NULL;
            dlcnt--;

            needupdate = true;
            nucnt++;
        }
        else
            sceKernelDelayThread(100000);
    }

    //sceHttpDeleteConnection(connid);
    sceHttpDeleteTemplate(templid);
    
    return 0;
}


Tile tiles[16];

/**
 * Computes the location on a great circle arc with the given starting location, azimuth, and arc distance.
 *
 * @param p LatLon of the starting location
 * @param greatCircleAzimuth great circle azimuth angle (clockwise from North)
 * @param pathLength arc distance to travel
 * @return LatLon location on the great circle arc.
 */
void greatCircleEndPosition(float pointlat, float pointlon, float azimuth, float distance, float *outlat, float *outlon) {
    float lat = pointlat, lon = pointlon;

    float cla = x_cosf(lat), sla = x_sinf(lat), sdi = x_sinf(distance);

    // Taken from "Map Projections - A Working Manual", page 31, equation 5-5 and 5-6.
    float endLatRadians = x_asinf(sla * x_cosf(distance) + cla * sdi * x_cosf(azimuth));
    float endLonRadians = lon + x_atan2f(sdi * x_sinf(azimuth), cla * x_cosf(distance) - sla * sdi * x_cosf(azimuth));

    lat = x_fmodf(endLatRadians, PI);
    *outlat = (float) (lat > PI / 2 ? PI - lat : lat < -PI / 2 ? -PI - lat : lat);

    lon = x_fmodf(endLonRadians, 2 * PI);
    *outlon = (float) (lon > PI ? lon - 2 * PI : lon < -PI ? 2 * PI + lon : lon);
}


float anglex, angley;

void updateBounds() {
    if (vcy > 90) {
        targetvcy = vcy = 90;
    } else if (vcy < -90) {
        targetvcy = vcy = -90;
    }

    if (vcx > 180) {
        vcx = vcx - 360;
        targetvcx = targetvcx - 360;
    } else if (vcx < -180) {
        vcx = vcx + 360;
        targetvcx = targetvcx + 360;
    }

    xrot = vcy*DE2RA;
    yrot = -vcx*DE2RA;

    float fovy = 75.0 * zfv, fovx = x_atanf(480.0f / 272.0f * x_tanf(fovy / 2.0f * DE2RA))*2.0f * RA2DE;
    
    float ss = (R + zm) / R * x_sinf(fovy / 2.0 * DE2RA);

    if (ss > 1)
        angley = x_acosf(R/(R+zm))*RA2DE * 2;
    else
        angley = (PI - fovy / 2.0f * DE2RA - (PI - x_asinf(ss)))*2.0f * RA2DE;
        
    
    ss = (R + zm) / R * x_sinf(fovx * 0.5f * DE2RA);
    if (ss > 1)
        anglex = x_acosf(R/(R+zm))*RA2DE * 2;
    else
        anglex = (PI - fovx * 0.5f * DE2RA - (PI - x_asinf(ss)))*2.0f * RA2DE;

    float d = x_sqrtf(480*480+272*272), angled;
    float fovd = x_atanf(d/272.0f * x_tanf(fovy / 2.0f * DE2RA))*2.0f * RA2DE;
    ss = (R + zm) / R * x_sinf(fovd * 0.5f * DE2RA);
    if (ss > 1)
        angled = x_acosf(R/(R+zm))*RA2DE * 2;
    else
        angled = (PI - fovd * 0.5f * DE2RA - (PI - x_asinf(ss)))*2.0f * RA2DE;
       
    float vlx1 = 1000,  vly1 = 1000, vrx1 = -1000, vry1 = -1000;

    float aa, bb;

    for (float a = 0; a < TWOPI; a += PI * 0.1f) {
        greatCircleEndPosition(xrot, -yrot, a, angled * DE2RA * 0.5f, &aa, &bb);

        if (aa < vly1) vly1 = aa;
        if (aa > vry1) vry1 = aa;
        if (bb < vlx1) vlx1 = bb;
        if (bb > vrx1) vrx1 = bb;
    }


    vlx = vlx1*RA2DE;
    vly = vly1*RA2DE;
    vrx = vrx1*RA2DE;
    vry = vry1*RA2DE;
}

Tile *updqueue[100];
int updqcnt;


int updater(SceSize args, void *argp) {
    pspvfpu_initcontext();

    
   bool f;
    while (!exiting) {
        updcnt = 0;
        updqcnt = 0;

        if (needupdate)
        {
            needupdate = false;

            for (int i = 0; i < 16; i++)
            tiles[i].update();
        }

        do {
            f = false;
            int bi = -1;
            unsigned int bl = 100;
            float bd = 1000.0f;

            for (int i = 0; i < updqcnt; i++) {
                Tile *t = updqueue[i];
                if (!t)
                    continue;
                float dist = x_fabsf(t->cx - vcx) + x_fabsf(t->cy - vcy);
                if ((t->tlev < bl) || (t->tlev == bl && dist < bd))
                //if (dist < bd)
                {
                    bi = i;
                    bl = t->tlev;
                    bd = dist;
                }
            }

            if (bi != -1) {
                updqueue[bi]->loadTile();
                updqueue[bi] = NULL;
                f = true;
            }
        } while (f);

        sceKernelDelayThread(50000);
    }
    
    return 0;
}

int scode;

void netInit() {
    sceUtilityLoadNetModule(PSP_NET_MODULE_COMMON);
    sceUtilityLoadNetModule(PSP_NET_MODULE_INET);
    sceUtilityLoadNetModule(PSP_NET_MODULE_PARSEURI);
    sceUtilityLoadNetModule(PSP_NET_MODULE_PARSEHTTP);
    sceUtilityLoadNetModule(PSP_NET_MODULE_HTTP);

    int res;

    res = sceNetInit(0x20000, 0x2A, 0, 0x2A, 0);
    sceNetInetInit();
    sceNetResolverInit();
    sceNetApctlInit(0x8000, 48);
    sceHttpInit(0x25800);
    sceHttpLoadSystemCookie();
}

int connect_to_apctl(int config) {
    int err;
    int stateLast = -1;


    err = sceNetApctlConnect(config);
    if (err != 0)
        return 0;

    while (1) {
        int state;
        err = sceNetApctlGetState(&state);
        if (err != 0)
            break;

        if (state != stateLast)
            stateLast = state;
        if (state == 4)
            break;
        sceKernelDelayThread(50 * 1000);
    }

    sceKernelDelayThread(3000 * 1000);

    return err == 0;
}

int netDialog() {
    int done = 0;

    pspUtilityNetconfData data;

    memset(&data, 0, sizeof (data));
    data.base.size = sizeof (data);
    data.base.language = PSP_SYSTEMPARAM_LANGUAGE_ENGLISH;
    data.base.buttonSwap = PSP_UTILITY_ACCEPT_CROSS;
    data.base.graphicsThread = 17;
    data.base.accessThread = 19;
    data.base.fontThread = 18;
    data.base.soundThread = 16;
    data.action = PSP_NETCONF_ACTION_CONNECTAP;

    struct pspUtilityNetconfAdhoc adhocparam;
    memset(&adhocparam, 0, sizeof (adhocparam));
    data.adhocparam = &adhocparam;

    sceUtilityNetconfInitStart(&data);

    while (!exiting) {
        sceGuStart(GU_DIRECT, list);
        sceGuClearColor(0xff554433);
        sceGuClear(GU_COLOR_BUFFER_BIT | GU_DEPTH_BUFFER_BIT);
        sceGuFinish();
        sceGuSync(0, 0);

        switch (sceUtilityNetconfGetStatus()) {
            case PSP_UTILITY_DIALOG_NONE:
                break;

            case PSP_UTILITY_DIALOG_VISIBLE:
                sceUtilityNetconfUpdate(1);
                break;

            case PSP_UTILITY_DIALOG_QUIT:
                sceUtilityNetconfShutdownStart();
                break;

            case PSP_UTILITY_DIALOG_FINISHED:
                done = 1;
                break;

            default:
                break;
        }

        sceDisplayWaitVblankStart();
        sceGuSwapBuffers();

        if (done)
            break;
    }

    return 1;
}

typedef enum {
    NoAction,
    
    LayerYMSat,
    LayerVESat,
    LayerGMTer,
    LayerGMMap,
    LayerOSMMapnik,
    
    LayerGMoonVis,
    LayerGMoonElev,
    
    LayerGMarsVis,
    LayerGMarsInfra,
    LayerGMarsElev,
    
    VExag0,
    VExag50,
    VExag100,
    
    PlacesSave,
    
    HtmlTest,
} MenuAction;

#define MA_BASE_LAYER LayerYMSat
#define MA_BASE_PLACES 1000

MenuContext* context;

LAYER layers[] = {
    { "Yahoo Maps - Satellite", "ymsat", "http://aerial.maps.yimg.com/ximg?t=a&v=1.8&s=256&x=%d&y=%d&z=%d", "(c) Yahoo", URL_YAHOO, IMG_JPG, false, true, 0 },
    { "Virtual Earth - Satellite", "vesat", "http://h%d.ortho.tiles.virtualearth.net/tiles/a%s.jpeg?g=1", "(c) Microsoft", URL_QKEY, IMG_JPG, false, true, 0 },
    { "Google Maps - Terrain", "gmter", "http://mt%d.google.com/mt?v=w2p.87&hl=en&x=%d&s=&y=%d&z=%d&s=G", "(c) Google", URL_XYZ, IMG_JPG, false, true, 0 },
    { "Google Maps - Map", "gmmap", "http://mt%d.google.com/mt?v=w2.88&hl=en&x=%d&s=&y=%d&z=%d&s=G", "(c) Google", URL_XYZ, IMG_PNG, true, true, 0 },
    { "OpenStreetMap - Mapnik", "osmma", "http://%c.tile.openstreetmap.org/%d/%d/%d.png", "(c) OpenStreetMap", URL_OSM, IMG_PNG, true, true, 0 },
    
    { "Moon - Visible + Apollo", "moonv", "http://mw1.google.com/mw-planetary/lunar/lunarmaps_v1/apollo/%d/%d/%d.jpg", "(c) Google", URL_GMOON, IMG_JPG, false, false, 1 },
    { "Moon - Elevation", "moone", "http://mw1.google.com/mw-planetary/lunar/lunarmaps_v1/terrain/%d/%d/%d.jpg", "(c) Google", URL_GMOON, IMG_JPG, false, false, 1 },
    
    { "Mars - Visible", "marsv", "http://mw1.google.com/mw-planetary/mars/visible/t%s.jpg", "(c) Google", URL_GMARS, IMG_JPG, false, false, 2 },
    { "Mars - Infrared", "marsi", "http://mw1.google.com/mw-planetary/mars/infrared/t%s.jpg", "(c) Google", URL_GMARS, IMG_JPG, false, false, 2 },
    { "Mars - Elevation", "marse", "http://mw1.google.com/mw-planetary/mars/elevation/t%s.jpg", "(c) Google", URL_GMARS, IMG_JPG, false, false, 2 },
    
    { NULL }
};
//sprintf (url, "http://khm%d.google.com/kh?v=33&hl=en&x=%d&y=%d&z=%d", snum++, ti, ycnt-1-tj, tlev);

LAYER *curlayer;

BOOKMARK bookmarks_earth[] = 
    //earth
    {
        { "Mount Etna", 37.879356, 14.990329, 13.535317, 64.226463, 13.764601 },
        { "Taal Volcano", 14.011544, 120.998100, 4.400003, 0, 0 },
        { "Red Square", 55.792553, 37.621014, 0.291667, 0, 0 },
        { NULL }
    };

BOOKMARK bookmarks_moon[] = 
    //moon
    {
        { "Apollo 11 landing site", 0.656424, 23.471662, 5.535317, 0, 0 },
        { NULL }
    };

BOOKMARK bookmarks_mars[] = 
    //mars
    {
        { "Mars 2 Lander (USSR)", -38.822590, 45.703125, 30, 0, 0 },
        { NULL }
    };
    
BOOKMARK *bookmarks[] = { bookmarks_earth, bookmarks_moon, bookmarks_mars };

void load_bookmarks()
{
    FILE *f = fopen("bookmarks.txt", "r");
    
    if (!f)
        return;
        
    char str[200];
        
    while(fgets(str, sizeof(str), f))
    {
        str[20] = 0;
        addMenuItem(context, 0, createTriggerButton(str), HtmlTest, 0);
    }
    
    fclose(f);
}

void SetupMenu() {
    context = initMenu();
    MenuItem* m1 = addMenuItem(context, 0, createMenuContainer("Layer"), NoAction, 0);

    for (int i = 0; layers[i].name; i++)
        addMenuItem(context, m1, createRadioButton(layers[i].name, i==config.layer), MA_BASE_LAYER+i, 0);

    MenuItem* m4 = addMenuItem(context, 0, createMenuContainer("Vertical Exaggeration"), NoAction, 0);
    MenuItem* m5 = addMenuItem(context, m4, createRadioButton("0 (Flat Terrain)", config.vexagg==0), VExag0, 0);
    MenuItem* m6 = addMenuItem(context, m4, createRadioButton("5.0", config.vexagg==5), VExag50, 0);
    MenuItem* m7 = addMenuItem(context, m4, createRadioButton("10.0", config.vexagg==10), VExag100, 0);

    MenuItem* m8 = addMenuItem(context, 0, createMenuContainer("Places"), NoAction, 0);
//    MenuItem* m9 = addMenuItem(context, m8, createTriggerButton("Save"), PlacesSave, 0);
    for (int i = 0; bookmarks[curlayer->world][i].title; i++)
        addMenuItem(context, m8, createTriggerButton(bookmarks[curlayer->world][i].title), MA_BASE_PLACES+i, 0);

    //MenuItem* m10 = addMenuItem(context, 0, createTriggerButton("Test HTML"), HtmlTest, 0);
}

SceUID upduid;

SceUID vpl;
pspUtilityHtmlViewerParam params;

int htmlViewerInit(char *url) {
    int res;
    //	vpl = sceKernelCreateVpl("BrowserVpl", PSP_MEMORY_PARTITION_USER, 0, BROWSER_MEMORY + 256, NULL);

    //	if (vpl < 0)
    //	return 10;

    memset(&params, 0, sizeof (pspUtilityHtmlViewerParam));

    params.base.size = sizeof (pspUtilityHtmlViewerParam);

    sceUtilityGetSystemParamInt(PSP_SYSTEMPARAM_ID_INT_LANGUAGE, &params.base.language);
    sceUtilityGetSystemParamInt(PSP_SYSTEMPARAM_ID_INT_UNKNOWN, &params.base.buttonSwap);

    params.base.graphicsThread = 17;
    params.base.accessThread = 19;
    params.base.fontThread = 18;
    params.base.soundThread = 16;
    params.memsize = BROWSER_MEMORY;
    params.initialurl = url;
    params.numtabs = 1;
    params.cookiemode = PSP_UTILITY_HTMLVIEWER_COOKIEMODE_DEFAULT;
    params.homeurl = url;
    params.textsize = PSP_UTILITY_HTMLVIEWER_TEXTSIZE_NORMAL;
    params.displaymode = PSP_UTILITY_HTMLVIEWER_DISPLAYMODE_SMART_FIT;
    params.options = PSP_UTILITY_HTMLVIEWER_DISABLE_STARTUP_LIMITS | PSP_UTILITY_HTMLVIEWER_ENABLE_FLASH;
    params.interfacemode = PSP_UTILITY_HTMLVIEWER_INTERFACEMODE_FULL;
    params.connectmode = PSP_UTILITY_HTMLVIEWER_CONNECTMODE_MANUAL_ALL;
    params.disconnectmode = PSP_UTILITY_HTMLVIEWER_DISCONNECTMODE_DISABLE;

    // Note the lack of 'ms0:' on the paths
    params.dldirname = "/PSP/PHOTO";

    //res = sceKernelAllocateVpl(vpl, params.memsize, &params.memaddr, NULL);

    //if (res < 0)
    //	return 20;

    params.memaddr = malloc(BROWSER_MEMORY);
    
    if (!params.memaddr)
        return 40;

    res = sceUtilityHtmlViewerInitStart(&params);

    if (res < 0)
        return 30;

    return 0;
}

int updateHtmlViewer() {
    sceGuStart(GU_DIRECT, list);
    sceGuClear(GU_COLOR_BUFFER_BIT | GU_DEPTH_BUFFER_BIT);
    sceGuFinish();
    sceGuSync(0, 0);

    switch (sceUtilityHtmlViewerGetStatus()) {
        case PSP_UTILITY_DIALOG_VISIBLE:
            sceUtilityHtmlViewerUpdate(1);
            break;

        case PSP_UTILITY_DIALOG_QUIT:
            sceUtilityHtmlViewerShutdownStart();
            break;

        case PSP_UTILITY_DIALOG_NONE:
            return 0;
            break;

        default:
            break;
    }

    return 1;
}

bool inhtml = 0;
int htmlok;

enum colors {
    RED = 0xFF0000FF,
    GREEN = 0xFF00FF00,
    BLUE = 0xFFFF0000,
    WHITE = 0xFFFFFFFF,
    LITEGRAY = 0xFFBFBFBF,
    GRAY = 0xFF7F7F7F,
    DARKGRAY = 0xFF3F3F3F,
    BLACK = 0xFF000000
};

intraFont *fntTop;

float ta;


#define CONFIG_HDR "PSPVGCFGA"

void config_load()
{
    //defaults
    config.vexagg = 5;
    
    FILE *f = fopen("cfg", "r");
    
    if (!f)
        return;
        
    fseek(f, sizeof(CONFIG_HDR), SEEK_SET); //skip header
    fread(&config, sizeof(config), 1, f); //default layer
    
    fclose(f);
}

void config_save()
{
    FILE *f = fopen("cfg", "w");
    
    if (!f)
        return;
        
    fwrite(CONFIG_HDR, sizeof(CONFIG_HDR), 1, f);
    fwrite(&config, sizeof(config), 1, f);
        
    fclose(f);
}

extern int snum;
void switch_layer(int li)
{
    curlayer = &layers[li];
    snum = 0;
    
    index_open();
    
    for (int j = 0; j < 4; j++)
    {
        for (int i = 0; i < 4; i++)
        {
            tiles[j*4+i].initTile(2, i, j);
            tiles[j*4+i].indo = (j*4+i)*sizeof(INDEX_DATA) + sizeof(INDEX_HDR_IND);
            tiles[j*4+i].loadTile();
        }
    }

    dldruid = sceKernelCreateThread("dldr", dldr2, 0x20, 0x10000, 0, NULL);
    sceKernelStartThread(dldruid, 0, NULL);
    
    upduid = sceKernelCreateThread("updater", updater, 0x25, 0x10000, 0, NULL);
    sceKernelStartThread(upduid, 0, NULL);
    
    needupdate = true;
}

int main(int argc, char **argv) {
    fbp0 = 0;

    tickResolution = sceRtcGetTickResolution();

    CTimer timer;
    SceCtrlData pad, lastPad;
    sceCtrlReadBufferPositive(&lastPad, 1);

    SetupCallbacks();
    pspDebugScreenInit();
    sceKernelDcacheWritebackAll();

    mkdir("Data", 0777);

    char d[50];
    for (int i = 0; layers[i].name; i++)
    {
        sprintf(d, "Data/%s", layers[i].dir);
        mkdir(d, 0777);
    }

    config_load();
    memmgr_init();

    intraFontInit();
    fntTop = intraFontLoad("flash0:/font/ltn0.pgf", INTRAFONT_CACHE_ASCII);
    intraFontSetStyle(fntTop, 0.5f, WHITE, BLACK, 0);

    netInit();
    InitGU();

    netDialog();
    //connect_to_apctl(1);
    
    updateBounds();

    vexaggeration = VEXAGGERATION_BASE * config.vexagg;
    switch_layer(config.layer);

    SetupMenu();
    //load_bookmarks();

    timer.GetDeltaTime();
    while (!exiting) {
        if (inhtml) {
            if (updateHtmlViewer() == 0) {
                inhtml = false;
            } else {
                sceDisplayWaitVblankStart();
                sceGuSwapBuffers();
            }

            continue;
        }

        float dt = timer.GetDeltaTime();

        sceCtrlPeekBufferPositive(&pad, 1);

        MenuItem* action = handleMenu(context, &pad);

        if (action) {
            switch (action->id) {
                case HtmlTest:
                {
                    char url[] = "http://www.ps2dev.org/";
                    htmlok = htmlViewerInit(url);

                    if (!htmlok)
                        inhtml = true;
                    else
                        targetvcx = htmlok;
                    break;
                }

                case PlacesSave:
                    printf ("%f %f %f %f %f\n", targetvcy, targetvcx, targetzm, -trot*RA2DE, krot*RA2DE);
                    break;
                case LayerVESat:
                case LayerGMTer:
                case LayerGMMap:
                case LayerYMSat:
                case LayerOSMMapnik:
                case LayerGMoonVis:
                case LayerGMoonElev:
                case LayerGMarsVis:
                case LayerGMarsInfra:
                case LayerGMarsElev:
                {
                    sceKernelTerminateDeleteThread(dldruid);
                    sceKernelTerminateDeleteThread(upduid);
                    
                    index_close();

                    for (int i = 0; i < 16; i++)
                        tiles[i].deleteTile();
                        
                    dltile = NULL;
                    dlcnt = 0;
                    
                    char oldworld = curlayer->world;

                    config.layer = action->id - MA_BASE_LAYER;
                    switch_layer(config.layer);
                    config_save();
                    
                    if (curlayer->world != oldworld)
                    {
                        destroyMenu(context);
                        SetupMenu();
                        zm = targetzm = 2000;
                        krot = trot = 0;
                        vcx = vcy = targetvcx = targetvcy = 0;
                        updateBounds();
                    }
                    
                    break;
                }

                case VExag0:
                case VExag50:
                case VExag100:
                {
                    
                    if (action->id == VExag0)
                        config.vexagg = 0;
                    else
                        config.vexagg = action->id == VExag50 ? 5.0f : 10.0f;
                        
                    vexaggeration = VEXAGGERATION_BASE * config.vexagg;

                    for (int i = 0; i < 16; i++)
                        tiles[i].RecreateMesh();                    
                    sceKernelDcacheWritebackAll();
                    
                    config_save();
                    
                    break;
                }

            }
            
            if (action->id >= MA_BASE_PLACES)
            {
                BOOKMARK *b = &bookmarks[curlayer->world][action->id - MA_BASE_PLACES];
                targetvcx = b->lon;
                targetvcy = b->lat;
                targetzm = b->zm;
                krot = b->azimuth*DE2RA;
                trot = -b->tilt*DE2RA;

                context->open = false;
                ef = MOVESPEED_FLY;
            }
        }
        
        if (pad.Buttons & PSP_CTRL_SELECT && !(lastPad.Buttons & PSP_CTRL_SELECT))
        {
            flat = !flat;
        }

        if (pad.Buttons & PSP_CTRL_CROSS) {
            if (trot > -PI / 2 + PI / 8)
                trot -= 0.02f * dt * 60.0f; ///0.017f;
        }
        if (pad.Buttons & PSP_CTRL_TRIANGLE) {
            if (trot < 0)
                trot += 0.02f * dt * 60.0f;
        }
        if (pad.Buttons & PSP_CTRL_SQUARE) {
            krot += 0.04f * dt * 60.0f;
        }
        if (pad.Buttons & PSP_CTRL_CIRCLE) {
            krot -= 0.04f * dt * 60.0f;
        }

        if (pad.Buttons & PSP_CTRL_LTRIGGER) {
            if (targetzm < R * 3)
                targetzm += targetzm * 0.05f;
            ef = MOVESPEED_NORMAL;
        }
        if (pad.Buttons & PSP_CTRL_RTRIGGER) {
            if (targetzm > 0.001)
                targetzm -= targetzm * 0.05f;
            ef = MOVESPEED_NORMAL;
        }
        if (pad.Buttons & PSP_CTRL_UP) {
            targetvcy += anglex*0.02f * x_cosf(krot) * dt * 60.0f;
            targetvcx += anglex*0.02f * x_sinf(krot) * dt * 60.0f;
            ef = MOVESPEED_NORMAL;
        }
        if (pad.Buttons & PSP_CTRL_DOWN) {
            targetvcy -= anglex*0.02f * x_cosf(krot) * dt * 60.0f;
            targetvcx -= anglex*0.02f * x_sinf(krot) * dt * 60.0f;
            ef = MOVESPEED_NORMAL;
        }
        if (pad.Buttons & PSP_CTRL_LEFT) {
            targetvcy += anglex*0.02f * x_sinf(krot) * dt * 60.0f;
            targetvcx -= anglex*0.02f * x_cosf(krot) * dt * 60.0f;
            ef = MOVESPEED_NORMAL;
        }
        if (pad.Buttons & PSP_CTRL_RIGHT) {
            targetvcy -= anglex*0.02f * x_sinf(krot) * dt * 60.0f;
            targetvcx += anglex*0.02f * x_cosf(krot) * dt * 60.0f;
            ef = MOVESPEED_NORMAL;
        }
        lastPad = pad;

        bool uf = false;
        if (targetzm != zm)
        {
            if (x_fabsf(targetzm - zm) < 0.0001f*zm)
                zm = targetzm;
            else
                zm += (targetzm - zm) * ef * dt * 60.0f;
            uf = true;
            nucnt++;
        }

        if (targetvcx != vcx)
        {
            if (x_fabsf(targetvcx - vcx) < 0.0001f*anglex)
                vcx = targetvcx;
            else
                vcx += (targetvcx - vcx) * ef * dt * 60.0f;
            uf = true;
        }

        if (targetvcy != vcy)
        {
            if (x_fabsf(targetvcy - vcy) < 0.0001f*angley)
                vcy = targetvcy;
            else
                vcy += (targetvcy - vcy) * ef * dt * 60.0f;
            uf = true;
        }

        if (zm > 25)
            flat = false;
        else
            flat = true;
        
        if (zm < 20 && 0)
        {
            ta = GetElevation(targetvcy*DE2RA, targetvcx*DE2RA) *vexaggeration;
            if (ta+0.2 > zm)
                targetzm = zm = ta + 0.2;
        }
        
        if (uf)
        {
            updateBounds();
            needupdate = true;
        }

        DrawScene();
        listsize = sceGuCheckList();

        pspDebugScreenSetOffset((int) fbp0);
        renderMenu(context, 0, 2);

        sceDisplayWaitVblankStart();
        fbp0 = sceGuSwapBuffers();
    }
    
    index_close();
    
//    sceKernelTerminateDeleteThread(dldruid);
//    sceKernelTerminateDeleteThread(upduid);

    sceGuTerm();

	sceHttpSaveSystemCookie();
	sceHttpEnd();
	sceNetApctlTerm();
	sceNetInetTerm();
	sceNetTerm();
    sceUtilityLoadNetModule(PSP_NET_MODULE_HTTP);
    sceUtilityLoadNetModule(PSP_NET_MODULE_PARSEHTTP);
    sceUtilityLoadNetModule(PSP_NET_MODULE_PARSEURI);
    sceUtilityLoadNetModule(PSP_NET_MODULE_INET);
    sceUtilityLoadNetModule(PSP_NET_MODULE_COMMON);

    sceKernelExitGame();
    return 0;
}

void InitGU(void) {
    sceGuInit();
    sceGuStart(GU_DIRECT, list);

    void* fbp0 = getStaticVramBuffer(BUF_WIDTH, SCR_HEIGHT, GU_PSM_8888); // drawbuffer 1, 512x272 (480x272 visible)
    void* fbp1 = getStaticVramBuffer(BUF_WIDTH, SCR_HEIGHT, GU_PSM_8888); // drawbuffer 2
    void* zbp = getStaticVramBuffer(BUF_WIDTH, SCR_HEIGHT, GU_PSM_4444); // zbuffer, always 16bit

    sceGuDrawBuffer(GU_PSM_8888, fbp0, BUF_WIDTH);
    sceGuDispBuffer(SCR_WIDTH, SCR_HEIGHT, fbp1, BUF_WIDTH);
    sceGuDepthBuffer(zbp, BUF_WIDTH);

    sceGuOffset(2048 - (SCR_WIDTH / 2), 2048 - (SCR_HEIGHT / 2));
    sceGuViewport(2048, 2048, SCR_WIDTH, SCR_HEIGHT);
    sceGuDepthRange(0, 65535);
    sceGuScissor(0, 0, SCR_WIDTH, SCR_HEIGHT);
    sceGuEnable(GU_SCISSOR_TEST);
    sceGuDepthFunc(GU_LESS);
    sceGuEnable(GU_DEPTH_TEST);
    sceGuClearDepth(65535);
    sceGuFrontFace(GU_CW);
    sceGuEnable(GU_CULL_FACE);
    sceGuShadeModel(GU_SMOOTH);
    sceGuDisable(GU_CLIP_PLANES);
    sceGuEnable(GU_TEXTURE_2D);
    sceGuEnable(GU_LIGHT0); // Enable Light 1 (NEW)

    sceGuTexMode(GU_PSM_8888, 0, 0, GU_TRUE);
    sceGuTexFunc(GU_TFX_MODULATE, GU_TCC_RGB);
    sceGuTexWrap(GU_CLAMP, GU_CLAMP);
    sceGuTexFilter(GU_LINEAR, GU_LINEAR);
    sceGuTexScale(1.0f, 1.0f); // No scaling
    sceGuTexOffset(0.0f, 0.0f);
    
    ScePspFVector3 lightPosition = {0.0f, 0.0f, 2.0f};
    sceGuLight(0, GU_POINTLIGHT, GU_AMBIENT_AND_DIFFUSE, &lightPosition);
    sceGuLightColor(0, GU_AMBIENT, GU_COLOR(0.25f, 0.25f, 0.25f, 1.0f));
    sceGuLightColor(0, GU_DIFFUSE, GU_COLOR(1.0f, 1.0f, 1.0f, 1.0f));

    sceGuFinish();
    sceGuSync(0, 0);

    sceDisplayWaitVblankStart();
    sceGuDisplay(GU_TRUE);
}

bool aa = true;

int rendcnt;
int lload = false;
extern int memmgr_usedcnt;

void DrawScene(void) {
    float zm0 = zm;
    if (flat)
        zm0 *= SCALE;

    sceGumMatrixMode(GU_PROJECTION);
    {
        sceGumLoadIdentity();
        sceGumPerspective(75.0f * zfv, 16.0f / 9.0f, flat ? zm0/10 : 0.1f, zm0 + flat ? R*1.5f*SCALE : R*1.5f);
    }

    sceGumMatrixMode(GU_VIEW);
    {
        sceGumLoadIdentity();
        ScePspFVector3 move4 = {0.0f, 0, -R - zm0};
        sceGumTranslate(&move4);
    }

    sceGumMatrixMode(GU_MODEL);
    {
        sceGumLoadIdentity();
    
        ScePspFVector3 move = {0.0f, 0.0f, R};
        sceGumTranslate(&move);
    
        ScePspFVector3 rot2 = {trot, 0, krot};
        sceGumRotateXYZ(&rot2);
    
        ScePspFVector3 move3 = {0.0f, 0.0f, -R};
        sceGumTranslate(&move3);
    
        if (!flat)
        {
            ScePspFVector3 rot = {xrot, yrot, 0};
            sceGumRotateXYZ(&rot);
        }
        else
        {
            ScePspFVector3 scale1 = {SCALE*x_cosf(vcy*DE2RA), SCALE, 1};
            sceGumScale(&scale1);
            ScePspFVector3 move5 = {-vcx*DE2RA*R, -vcy*DE2RA*R, 0};
            sceGumTranslate(&move5);
        }
    }

    sceGuStart(GU_DIRECT, list);
    sceGuClearColor(0xFF000000);
    sceGuClear(GU_COLOR_BUFFER_BIT | GU_STENCIL_BUFFER_BIT | GU_DEPTH_BUFFER_BIT);

    sceGuTexMode(GU_PSM_8888, 0, 0, GU_TRUE);
    sceGuTexFunc(GU_TFX_MODULATE, GU_TCC_RGB);
    sceGuTexWrap(GU_CLAMP,GU_CLAMP);

    rendcnt  = 0;
                for (int i = 0; i < 16; i++)
            {            
            tiles[i].render();
            }

    /*
    tiles[0].render();
    tiles[1].render();
    tiles[2].render();
    tiles[3].render();*/

    sceGuEnable(GU_BLEND);
    sceGuBlendFunc(GU_ADD, GU_SRC_ALPHA, GU_ONE_MINUS_SRC_ALPHA, 0, 0);
    //intraFontPrintf(fntTop, 1, 8, "Lat: %.3f  Lon: %.3f  %.2f..%.2f %.2f..%.2f m=%i %i %i %i %i %i", vcy, vcx, vlx, vrx, vly, vry, (int)mem/1024, updcnt, nucnt, (int)lload, memmgr_usedcnt, rendcnt);
    intraFontPrintf(fntTop, 1, 8, "Lat: %.3f  Lon: %.3f", vcy, vcx);

    if (dlcnt) {
        if (aa) {
            intraFontSetStyle(fntTop, 0.5f, WHITE, BLACK, INTRAFONT_ALIGN_RIGHT);
            intraFontPrint(fntTop, 478, 8, "DL");
            intraFontSetStyle(fntTop, 0.5f, WHITE, BLACK, 0);
        }

        aa = !aa;
    }
    
    intraFontSetStyle(fntTop, 0.4f, WHITE, BLACK, INTRAFONT_ALIGN_RIGHT);
    intraFontPrint(fntTop, 478, 271, curlayer->copystr);
    intraFontSetStyle(fntTop, 0.5f, WHITE, BLACK, 0);
    sceGuDisable(GU_BLEND);

    sceGuFinish();
    sceGuSync(0, 0);
}
