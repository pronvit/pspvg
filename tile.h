#ifndef _H_TILE
#define _H_TILE

#define DRAW_OPTIONS GU_TEXTURE_32BITF | GU_COLOR_8888 | GU_VERTEX_32BITF | GU_INDEX_16BIT | GU_TRANSFORM_3D
#define SCALE 10000

#include "memmgr.h"

class Tile {
public:
//	unsigned char *pixels;
	int w, h;
	float lx, rx, ly, ry;
//	Vertex *vs, *vsf;
	unsigned int ti, tj, tlev;
	Tile *childs[4];
	float dx, dy;
	bool vsready;
	bool bad, noimg;
	bool dl;
	bool dld;
	char path[100];
	char url[200];
	bool nonlocal;
	int dlsize;
    float cx, cy;
    bool vis;
    Tile *parent;
    int NumLatitudes, NumLongitudes;
    TileMem *mem;
    INDEX_DATA ind;
    int indo;
	
public:

	void initTile(int _lev, int _i, int _j);
	void freeTile();
    void deleteTile();
	void loadTile();
	bool loadImage();
	void CreateTileVs();
	void RecreateMesh();
	bool isVisible();
	void update();
	bool render();
	void qkey(char *b, char *chs);
	void download();
};

float GetElevation(float lat, float lon);

bool PointInFrustum( float x, float y, float z );
bool FrustumPlaneOk(int p, float x, float y, float z);

bool SphereInFrustum( float x, float y, float z, float radius );

#endif
