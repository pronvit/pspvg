#include <malloc.h>
#include <stdio.h>

#include "memmgr.h"

TileMem *memory;
int memmgr_usedcnt;

extern int mem;

#define CACHE_SIZE 70

void memmgr_init()
{
    memory = (TileMem*)malloc(CACHE_SIZE*sizeof(TileMem));
    mem += CACHE_SIZE*sizeof(TileMem);
    
    for (int i = 0; i < CACHE_SIZE; i++)
    {
        memory[i].free = true;
    }
}

TileMem* memmgr_get()
{
    for (int i = 0; i < CACHE_SIZE; i++)
    {
        if (memory[i].free)
        {
            memory[i].free = false;
            memmgr_usedcnt++;
            return &memory[i];
        }
    }
    
    return NULL;
}

void memmgr_release(TileMem *mem)
{
    mem->free = true;
    memmgr_usedcnt--;
}
