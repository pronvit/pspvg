#ifndef vfpu_h
#define vfpu_h

#ifdef __cplusplus
extern "C" {
#endif

#define PI         3.14159265358979f
#define TWOPI      6.28318530717958f
#define DE2RA      0.01745329251994f
#define RA2DE      57.2957795130823f
#define PIOVER2    1.57079632679489f
#define PIOVER4    0.78539816339744f

float x_fabsf(float x);

float x_sinf(float x);
float x_cosf(float x);
void x_sincosf(float r, float *s, float *c);
float x_tanf(float x);

float x_acosf(float x);
float x_asinf(float x);
float x_atanf(float x);

float x_atan2f(float y, float x);

float x_expf(float x);
float x_logf(float x);

float x_sqrtf(float x);
float x_fmodf(float x, float y);

#ifdef __cplusplus
}
#endif

#endif
