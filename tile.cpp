#include <malloc.h>
#include <math.h>
#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspvfpu.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>

#include "tile.h"
#include "vfpu.h"
#include "index.h"
extern "C" {
#include "jpeglib.h"
#include "png/png.h"
}

int snum;

extern SceUID dldruid;
extern SceUID dlsema;
extern bool flat;

extern Tile *updqueue[];
extern int updqcnt;
extern float vexaggeration;

extern float vcx, vcy, vlx, vly, vrx, vry;
extern float anglex, angley;
extern int updcnt, dlcnt;
extern int fload, fmem;
extern int mem;
extern float R;
extern Tile *dltile, *dltile2;
extern LAYER *curlayer;

extern int lcnt;

typedef struct {
    short data[150 * 150];
} elevtile;

elevtile *elevtiles[18][9];

float GetElevation(float lat, float lon) {
    lat *= RA2DE;
    lon *= RA2DE;

    lat += 90;
    lon += 180;

    int tx = lon / (20);
    int ty = lat / (20);

    if (!elevtiles[tx][ty]) {
        char path[100];
        sprintf(path, "Data/srtm/%04d_%04d.bil", ty, tx);

        FILE *f = fopen(path, "rb");
        if (f)
        {
            elevtile *et = (elevtile*) malloc(sizeof (elevtile));
            if (!et)
            {
                fclose(f);
                return 0;
            }
            mem += sizeof (elevtile);
            elevtiles[tx][ty] = et;

            int i;
            for (i = 0; i < 150 * 150; i++)
                fread(et->data + i, 2, 1, f);

            fclose(f);
        }
    }

    elevtile *et = elevtiles[tx][ty];
    if (!et)
        return 0;

    int tileHeight = 150;
    int tileWidth = 150;
    float sectorDeltaLat = 20;
    float sectorDeltaLon = 20;
    float dLat = ty * (20) + 20 - lat;
    float dLon = lon - tx * (20);
    float sLat = dLat / sectorDeltaLat;
    float sLon = dLon / sectorDeltaLon;

    int j = (int) ((tileHeight - 1) * sLat);
    int i = (int) ((tileWidth - 1) * sLon);
    int k = j * tileWidth + i;

    float eLeft = et->data[k];
    float eRight = i < (tileWidth - 1) ? et->data[k + 1] : eLeft;

    float dw = sectorDeltaLon / (tileWidth - 1);
    float dh = sectorDeltaLat / (tileHeight - 1);
    float ssLon = (dLon - i * dw) / dw;
    float ssLat = (dLat - j * dh) / dh;

    float eTop = eLeft + ssLon * (eRight - eLeft);

    if (j < tileHeight - 1 && i < tileWidth - 1) {
        eLeft = et->data[k + tileWidth];
        eRight = et->data[k + tileWidth + 1];
    }
    float eBot = eLeft + ssLon * (eRight - eLeft);
    float el = eTop + ssLat * (eBot - eTop);
    
    return el > 0 ? el : 0;
}

//TODO: move this to vfpu when this will be actually used

void NormalVector(float *p1, float *p2, float *p3, float *n) {
    float v1[3], v2[3], d;
    // calculate two vectors, using the middle point
    // as the common origin
    v1[0] = p3[0] - p1[0];
    v1[1] = p3[1] - p1[1];
    v1[2] = p3[2] - p1[2];
    v2[0] = p3[0] - p2[0];
    v2[1] = p3[1] - p2[1];
    v2[2] = p3[2] - p2[2];

    // calculate the cross product of the two vectors
    n[0] = v1[1] * v2[2] - v2[1] * v1[2];
    n[1] = v1[2] * v2[0] - v2[2] * v1[0];
    n[2] = v1[0] * v2[1] - v2[0] * v1[1];

    // normalize the vector
    d = (n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
    // try to catch very small vectors
    if (d < (float) 0.00000001f) {
        d = (float) 100000000.0f;
    } else {
        d = (float) 1.0f / x_sqrtf(d);
    }

    n[0] *= d;
    n[1] *= d;
    n[2] *= d;
}

void swizzle_24to32(unsigned char *out, const unsigned char *image, unsigned int imageWidth, unsigned int imageHeight) {
    unsigned int i, j, k, blockx, blocky, by, x, y, block_index, block_address;
    unsigned int rowblocks = imageWidth >> 2;

    for (j = 0, k = 0; j < imageHeight; j++) {
        blocky = j >> 3;
        y = (j - (blocky << 3)) << 4;
        by = blocky * rowblocks;

        for (i = 0; i < imageWidth << 2; i += 4) {
            blockx = i >> 4;
            x = (i - (blockx << 4));

            block_index = blockx + by;
            block_address = (block_index << 7) + x + y;

            out[block_address++] = image[k++];
            out[block_address++] = image[k++];
            out[block_address++] = image[k++];
            out[block_address] = 0xff;
        }
    }
}

void Tile::initTile(int _lev, int _i, int _j) {
    ti = _i;
    tj = _j;
    tlev = _lev;
    
    ind.imgoffset = 0;

    unsigned int d = 1 << tlev;
    dx = 360.0f / d;
    dy = 180.0f / d;

    lx = -180.0f + dx * ti;
    rx = -180.0f + dx * (ti + 1);

    float l1 = 2 * x_atanf(x_expf((tj) * TWOPI / d - PI)) - PIOVER2;
    float l2 = 2 * x_atanf(x_expf((tj + 1) * TWOPI / d - PI)) - PIOVER2;
    ly = l1*RA2DE;
    ry = l2*RA2DE;

    if (tj == 0) ly = -90;
    if (tj == d - 1) ry = 90;
    
    cx = (rx + lx)*0.5f;
    cy = (ry + ly)*0.5f;
    
    //NumLatitudes = (tlev == 1 ? 20 : 10), NumLongitudes = (tlev == 1 ? 40 : 20);
    NumLatitudes = 10;
    NumLongitudes = 20;
//    if (tlev > 10)
//        NumLatitudes = NumLongitudes = 6;

    sprintf(path, "Data/%s/%02d/world-%02d-%04d-%04d.jpg", curlayer->dir, tlev, tlev, tj, ti);
}

void Tile::deleteTile() {
    if (mem)
    {
        memmgr_release(mem);
        mem = NULL;
    }

    vsready = false;
    bad = false;
    dl = false;
    dld = false;
    nonlocal = false;

    if (childs[0]) {
        childs[0]->deleteTile();
        childs[1]->deleteTile();
        childs[2]->deleteTile();
        childs[3]->deleteTile();
        delete childs[0];
        delete childs[1];
        delete childs[2];
        delete childs[3];

        childs[0] = NULL;
    }
    
    ind.indoffset[0] = 0;
}

void Tile::freeTile() {
    if (mem) {
        vsready = false;
            
        memmgr_release(mem);
        mem = NULL;

        lcnt--;
    }
    
    bad = false;
    dl = false;
    dld = false;
    nonlocal = false;

    if (childs[0]) {
        childs[0]->freeTile();
        childs[1]->freeTile();
        childs[2]->freeTile();
        childs[3]->freeTile();
    }
}

typedef struct {
    struct jpeg_error_mgr pub;
    jmp_buf setjmp_buffer;
} my_error_mgr;

void my_error_exit(j_common_ptr cinfo) {
    (*cinfo->err->output_message) (cinfo);
    longjmp(((my_error_mgr*) cinfo->err)->setjmp_buffer, 1);
}

extern int lload;

unsigned char decbuf[256*256*3];

extern FILE *imgf;

int mempos;

void png_mem_read(png_structp png_ptr, png_bytep data, png_size_t length)
{
    char *dptr = (char*) png_get_io_ptr(png_ptr);
    memcpy(data, dptr+mempos, length);
    mempos += length;
}

bool Tile::loadImage() {
lload = 10;


    lload = 3;
    
    if (curlayer->imgtype == IMG_JPG)
    {
        struct jpeg_decompress_struct cinfo;
        my_error_mgr jerr;
        cinfo.err = jpeg_std_error(&jerr.pub);
        
        jerr.pub.error_exit = my_error_exit;
        if (setjmp(jerr.setjmp_buffer)) {
            jpeg_destroy_decompress(&cinfo);
            
            //can't decompress - bad file? don't write it to disk and set imgoffset=0
            ind.imgoffset = 0;
            index_write(this);
            lload = 2;
            return false;
        }
    
        jpeg_create_decompress(&cinfo);
        if (!dlsize)
        {
            fseek(imgf, ind.imgoffset, SEEK_SET);
            jpeg_stdio_src(&cinfo, imgf);
        }
        else
            jpeg_mem_src(&cinfo, mem->pixels, dlsize);
        
        jpeg_read_header(&cinfo, TRUE);
    
        w = cinfo.image_width;
        h = cinfo.image_height;
        
        //should set decomp params here
        cinfo.out_color_space = JCS_RGB;
        jpeg_start_decompress(&cinfo);
        
        JSAMPROW a;
        while (cinfo.output_scanline < cinfo.output_height) {
            a = decbuf + w * cinfo.output_components * cinfo.output_scanline;
            jpeg_read_scanlines(&cinfo, &a, 1);
        }
        
        jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);
    }
    else if (curlayer->imgtype == IMG_PNG)
    {
        png_structp png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        png_infop info_ptr = png_create_info_struct(png_ptr);
        png_infop end_info = png_create_info_struct(png_ptr);
        
        if (setjmp(png_jmpbuf(png_ptr)))
        {
            png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);

            //can't decompress - bad file? don't write it to disk and set imgoffset=0
            ind.imgoffset = 0;
            index_write(this);
            lload = 2;
            return false;
        }

        if (!dlsize)
        {
            fseek(imgf, ind.imgoffset, SEEK_SET);
            png_init_io(png_ptr, imgf);
        }
        else
        {
            mempos = 0;
            png_set_read_fn(png_ptr, mem->pixels, png_mem_read);
        }
        
        w = 256;
        h = 256;
        
        png_byte *row_pointers[256];
        
        for (int i=0; i<256; i++)
            row_pointers[i] = decbuf+256*3*i;
        png_set_rows(png_ptr, info_ptr, row_pointers);
        
        png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_EXPAND, NULL);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    }
    
    if (dlsize)
    {
        int imgo;
        
        fseek(imgf, 0, SEEK_END);
        imgo = ftell(imgf);
        
        fwrite(mem->pixels, dlsize, 1, imgf);
        fflush(imgf);
        
        ind.imgoffset = imgo;
        index_write(this);
        
        dlsize = 0;
    }
    
    swizzle_24to32(mem->pixels, decbuf, w, h);

lload = 2;
    return true;
}


void Tile::loadTile() {
    bool res = false;

lload = 1;

    if (!ind.indoffset[0])
        index_read(this);

    if (!mem)
    {
        dlsize = 0;
        mem = memmgr_get();
    }
    if (!mem)
        return;

    if (ind.imgoffset || dlsize)
    {
        lload = 2;
        res = loadImage();
        lload = 1;
    }

    if (!res) {
        if (dld) {
            fload++;
            bad = true;
            if (mem)
            {
                memmgr_release(mem);
                mem = NULL;
            }
        } else {
            nonlocal = true;
            if (dlcnt < 1)
            {
                lload = 3;
                download();
                lload = 1;
            }
        }
lload = 0;
        return;
    }

    lload = 4;
    CreateTileVs();
    lload = 1;
    
    lload = 5;
    sceKernelDcacheWritebackAll();
    lload = 1;
    vsready = true;
    
lload = 0;    
}

extern float zm;

void Tile::RecreateMesh()
{
    if (vsready)
    {
        CreateTileVs();
    }
    
    if (childs[0])
    {
        childs[0]->RecreateMesh();
        childs[1]->RecreateMesh();
        childs[2]->RecreateMesh();
        childs[3]->RecreateMesh();
    }
}

#define VERT(vx,vy,vz,tu,tv) \
    vp->x = vx; \
    vp->y = vy; \
    vp->z = vz; \
    vp->u = tu; \
    vp->v = tv; \
    (vp++)->color = 0xFFFFFFFF;

#define VERTF(vx,vy,vz,tu,tv) \
    vpf->x = vx; \
    vpf->y = vy; \
    vpf->z = vz; \
    vpf->u = tu; \
    vpf->v = tv; \
    (vpf++)->color = 0xFFFFFFFF;

#define VERTS(_st,_ct,_sp,_cp,_theta,_phi,el,tu,tv) \
        { if (tlev < 10) \
            VERT((R+el) * _sp * _ct, (R+el) * _st, (R+el) * _cp * _ct, tu, tv) \
        VERTF((_phi-p0)*R, (_theta-t0)*R, R+el*SCALE, tu,tv) }
    
        
#define QI(a,b,c,d) a,b,d,d,b,c

//row start, next row start
#define RIW(rs, nrs) \
    QI(rs+0,nrs+0,nrs+1,rs+1),\
    QI(rs+1,nrs+1,nrs+2,rs+2),\
    QI(rs+2,nrs+2,nrs+3,rs+3),\
    QI(rs+3,nrs+3,nrs+4,rs+4),\
    QI(rs+4,nrs+4,nrs+5,rs+5),\
    QI(rs+5,nrs+5,nrs+6,rs+6),\
    QI(rs+6,nrs+6,nrs+7,rs+7),\
    QI(rs+7,nrs+7,nrs+8,rs+8),\
    QI(rs+8,nrs+8,nrs+9,rs+9),\
    QI(rs+9,nrs+9,nrs+10,rs+10),\
    QI(rs+10,nrs+10,nrs+11,rs+11), \
    QI(rs+11,nrs+11,nrs+12,rs+12)

#define RIE(rs, nrs) \
    QI(rs+12,nrs+12,nrs+11,rs+11),\
    QI(rs+11,nrs+11,nrs+13,rs+13),\
    QI(rs+13,nrs+13,nrs+14,rs+14),\
    QI(rs+14,nrs+14,nrs+15,rs+15),\
    QI(rs+15,nrs+15,nrs+16,rs+16),\
    QI(rs+16,nrs+16,nrs+17,rs+17),\
    QI(rs+17,nrs+17,nrs+18,rs+18),\
    QI(rs+18,nrs+18,nrs+19,rs+19),\
    QI(rs+19,nrs+19,nrs+20,rs+20),\
    QI(rs+20,nrs+20,nrs+21,rs+21),\
    QI(rs+21,nrs+21,nrs+22,rs+22),\
    QI(rs+22,nrs+22,nrs+23,rs+23)
    
#define RIWS(rs,nrs) \
    QI(nrs+0,nrs+1,nrs+2,rs+0),\
    QI(rs+0,nrs+2,nrs+3,rs+1),\
    QI(rs+1,nrs+3,nrs+4,rs+2),\
    QI(rs+2,nrs+4,nrs+5,rs+3),\
    QI(rs+3,nrs+5,nrs+6,rs+4),\
    QI(rs+4,nrs+6,nrs+7,rs+5),\
    QI(rs+5,nrs+7,nrs+8,rs+6),\
    QI(rs+6,nrs+8,nrs+9,rs+7),\
    QI(rs+7,nrs+9,nrs+10,rs+8),\
    QI(rs+8,nrs+10,nrs+11,nrs+12)
    
#define RIES(rs,nrs) \
    QI(nrs+12,nrs+11,nrs+13,rs+9),\
    QI(rs+9,nrs+13,nrs+14,rs+10),\
    QI(rs+10,nrs+14,nrs+15,rs+11),\
    QI(rs+11,nrs+15,nrs+16,rs+12),\
    QI(rs+12,nrs+16,nrs+17,rs+13),\
    QI(rs+13,nrs+17,nrs+18,rs+14),\
    QI(rs+14,nrs+18,nrs+19,rs+15),\
    QI(rs+15,nrs+19,nrs+20,rs+16),\
    QI(rs+16,nrs+20,nrs+21,rs+17),\
    QI(rs+17,nrs+21,nrs+22,nrs+23)

#define RIWN(rs,nrs) \
    QI(rs+1,rs+0,nrs+0,rs+2),\
    QI(rs+2,nrs+0,nrs+1,rs+3),\
    QI(rs+3,nrs+1,nrs+2,rs+4),\
    QI(rs+4,nrs+2,nrs+3,rs+5),\
    QI(rs+5,nrs+3,nrs+4,rs+6),\
    QI(rs+6,nrs+4,nrs+5,rs+7),\
    QI(rs+7,nrs+5,nrs+6,rs+8),\
    QI(rs+8,nrs+6,nrs+7,rs+9),\
    QI(rs+9,nrs+7,nrs+8,rs+10),\
    QI(rs+10,nrs+8,rs+12,rs+11)

#define RIEN(rs,nrs) \
    QI(rs+11,rs+12,nrs+9,rs+13),\
    QI(rs+13,nrs+9,nrs+10,rs+14),\
    QI(rs+14,nrs+10,nrs+11,rs+15),\
    QI(rs+15,nrs+11,nrs+12,rs+16),\
    QI(rs+16,nrs+12,nrs+13,rs+17),\
    QI(rs+17,nrs+13,nrs+14,rs+18),\
    QI(rs+18,nrs+14,nrs+15,rs+19),\
    QI(rs+19,nrs+15,nrs+16,rs+20),\
    QI(rs+20,nrs+16,nrs+17,rs+21),\
    QI(rs+21,nrs+17,rs+23,rs+22)

unsigned short __attribute__((aligned(16))) inds10x20[] = {
    //south-west
    RIWS(0,18),
    RIW(18+24*0,18+24*1),
    RIW(18+24*1,18+24*2),
    RIW(18+24*2,18+24*3),
    RIW(18+24*3,18+24*4),
    RIW(18+24*4,18+24*5),
    RIWN(18+24*5,18+24*6),
        
    //south-east
    RIES(0,18),
    RIE(18+24*0,18+24*1),
    RIE(18+24*1,18+24*2),
    RIE(18+24*2,18+24*3),
    RIE(18+24*3,18+24*4),
    RIE(18+24*4,18+24*5),
    RIWN(18+24*5,18+24*6),

    //north-west
    RIWS(18+24*6,18+24*5),
    RIW(18+24*5,18*2+24*6),
    RIW(18*2+24*6,18*2+24*7),
    RIW(18*2+24*7,18*2+24*8),
    RIW(18*2+24*8,18*2+24*9),
    RIW(18*2+24*9,18*2+24*10),
    RIWN(18*2+24*10, 18*2+24*11),

    //north-east
    RIES(18+24*6,18+24*5),
    RIE(18+24*5,18*2+24*6),
    RIE(18*2+24*6,18*2+24*7),
    RIE(18*2+24*7,18*2+24*8),
    RIE(18*2+24*8,18*2+24*9),
    RIE(18*2+24*9,18*2+24*10),
    RIEN(18*2+24*10, 18*2+24*11),
};

short indcnt = sizeof(inds10x20) / sizeof(unsigned short);

void Tile::CreateTileVs() {
    float phi, theta;
    float st, ct, kt, tt;
    float sx = 1.0f / NumLongitudes, sy = 1.0f / NumLatitudes;
    float lon_incr = (rx - lx) / NumLongitudes*DE2RA;
    float t0 = ly*DE2RA, p0 = lx*DE2RA;
    float sp[NumLongitudes+1], cp[NumLongitudes+1];
    int d = 1 << tlev;
    int col, row;

    //cache sin, cosf of longitudes
    for (col = 0, phi = p0; col <= NumLongitudes; col++, phi += lon_incr)
        x_sincosf(phi, &sp[col], &cp[col]);

    //create vertices
    float elev = 0;  
    Vertex *vp = mem->vs, *vpf = mem->vsf;
    
    //BOTTOM
    tt = x_expf(tj * TWOPI / d - PI); theta = 2 * x_atanf(tt) - PIOVER2; st = 1 - 2/(1+tt*tt); ct = 2 * tt/(1+tt*tt);
    for (col=1, phi=p0+lon_incr; col < NumLongitudes; col++, phi+=lon_incr)
    {
        if (col == NumLongitudes/2)
            continue;
        VERTS(st, ct, sp[col], cp[col], theta, phi, 0, sx*col, 1)
    }

    //MAIN    
    for (row=0, kt=0; row <= NumLatitudes; row++, kt += sy) {
        tt = x_expf((tj+kt) * TWOPI / d - PI); theta = 2 * x_atanf(tt) - PIOVER2; st = 1 - 2/(1+tt*tt); ct = 2 * tt/(1+tt*tt);
        
        //LEFT
        VERTS(st, ct, sp[0], cp[0], theta, p0, 0, 0, 1-kt)
        
        //MAIN
        for (col=0, phi=p0; col <= NumLongitudes; col++, phi+=lon_incr)
        {
            if (tlev > 3 && vexaggeration > 0 && curlayer->usesrtm)
                elev = GetElevation(theta, phi) * vexaggeration;

            VERTS(st, ct, sp[col], cp[col], theta, phi, elev, sx*col, 1-kt)
                                   
            //HCENTER
            if (col == NumLongitudes/2)
                VERTS(st, ct, sp[col], cp[col], theta, phi, 0, 0.5f, 1-kt)
        }

        //RIGHT
        VERTS(st, ct, sp[NumLongitudes], cp[NumLongitudes], theta, rx*DE2RA, 0, 1, 1-kt);
        
        //VCENTER    
        if (row == NumLatitudes/2)
        {
            for (col=1, phi=p0+lon_incr; col < NumLongitudes; col++, phi += lon_incr)
            {
                if (col == NumLongitudes/2)
                    continue;
                VERTS(st, ct, sp[col], cp[col], theta, phi, 0, sx*col, 0.5f)
            }
        }
    }
    
    //TOP    
    for (col=1, phi=p0+lon_incr; col < NumLongitudes; col++, phi += lon_incr)
    {
        if (col == NumLongitudes/2)
            continue;
        VERTS(st, ct, sp[col], cp[col], theta, phi, 0, sx*col, 0)
    }
}

void Tile::update()
{
    if (noimg)
        return;

    if (rx < vlx || ry < vly || lx > vrx || ly > vry)
    {
        if (tlev > 2)
        {
            if (mem)    
                freeTile();
        } else if (!vsready && !dl && !(nonlocal && dlcnt >= 1) && !bad)
            loadTile();
        else if (childs[0]) {
            // TODO: in order to draw better far distances don't free low-level tiles
            // when we have enough memory. on Slims increase CACHE_SIZE and don't free
            // AND implement on demand freeing in memory manager, not here. and render all
            // available tiles up to some level
            childs[0]->freeTile();
            childs[1]->freeTile();
            childs[2]->freeTile();
            childs[3]->freeTile();
        }
        return;
    }

    updcnt++;

    if (!vsready && !dl && !(nonlocal && dlcnt >= 1) && !bad) {
        //loadTile();
            if (!ind.indoffset[0])
        index_read(this);

                    updqueue[updqcnt++] = this;
    }

    if (dx > (curlayer->z ? anglex : anglex * 0.75f) && tlev < 17) {
        if (!childs[0])
        {
            Tile *c0  = new Tile();
            childs[1] = new Tile();
            childs[2] = new Tile();
            childs[3] = new Tile();
                        
            c0->initTile(tlev + 1, ti * 2, tj * 2);
            childs[1]->initTile(tlev + 1, ti * 2 + 1, tj * 2);
            childs[2]->initTile(tlev + 1, ti * 2, tj * 2 + 1);
            childs[3]->initTile(tlev + 1, ti * 2 + 1, tj * 2 + 1);
            
            c0->indo = ind.indoffset[0];
            childs[1]->indo = ind.indoffset[1];
            childs[2]->indo = ind.indoffset[2];
            childs[3]->indo = ind.indoffset[3];
            
            childs[0] = c0;
        }

        if (vcx <= cx && vcy <= cy) {
            childs[0]->update();
            childs[1]->update();
            childs[2]->update();
            childs[3]->update();
        } else if (vcx >= cx && vcy <= cy) {
            childs[1]->update();
            childs[0]->update();
            childs[2]->update();
            childs[3]->update();
        } else if (vcx <= cx && vcy >= cy) {
            childs[2]->update();
            childs[1]->update();
            childs[0]->update();
            childs[3]->update();
        } else if (vcx >= cx && vcy >= cy) {
            childs[3]->update();
            childs[1]->update();
            childs[2]->update();
            childs[0]->update();
        }
    } else
    {
        if (childs[0])
        {
        childs[0]->freeTile();
        childs[1]->freeTile();
        childs[2]->freeTile();
        childs[3]->freeTile();
    }
    }



}

extern int rendcnt;
extern float zm;

bool Tile::isVisible()
{
    return  !(rx < vlx || ry < vly || lx > vrx || ly > vry);
}

bool Tile::render() {
    if (noimg)
        return false;
        
    TileMem *mem2 = mem;
    
    if (tlev == 2) {
        if (vsready) {
            sceGuTexImage(0, w, h, w, mem2->pixels);
            sceGuDepthMask(GU_TRUE);
            if (!flat)
                sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt, inds10x20, mem2->vs);
            else
            {
                sceGumPushMatrix();    
                ScePspFVector3 tr  = { lx*DE2RA*R, ly*DE2RA*R, 0.0f };
                sceGumTranslate(&tr);
                sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt, inds10x20, mem2->vsf);
                sceGumPopMatrix();
            }
            sceGuDepthMask(GU_FALSE);
            rendcnt++;
        }
        
        if (rx < vlx || ry < vly || lx > vrx || ly > vry)
            return true;

        if (dx > (curlayer->z ? anglex : anglex * 0.75f) && childs[0]) {
            childs[0]->render();
            childs[1]->render();
            childs[2]->render();
            childs[3]->render();
        }

        return true;
    }

    if (!vsready)
        return false;

//    if (rx < vlx || ry < vly || lx > vrx || ly > vry)
//        return false;

    bool rc0 = false, rc1 = false, rc2 = false, rc3 = false;

    if (dx > (curlayer->z ? anglex : anglex * 0.75f) && childs[0]) {
        rc0 = childs[0]->render();
        rc1 = childs[1]->render();
        rc2 = childs[2]->render();
        rc3 = childs[3]->render();
    }

    if (rc0 && rc1 && rc2 && rc3)
        return true;
    
    sceGuTexImage(0, w, h, w, mem2->pixels);    
    if (!(rc0 || rc1 || rc2 || rc3))
    {
        if (!flat)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt, inds10x20, mem2->vs);
        else
        {
            sceGumPushMatrix();    
            ScePspFVector3 tr  = { lx*DE2RA*R, ly*DE2RA*R, 0.0f };
            sceGumTranslate(&tr);
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt, inds10x20, mem2->vsf);
            sceGumPopMatrix();
        }
        return true;
    }
    
    if (!flat)
    {
        if (!rc0)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20, mem2->vs);
        if (!rc1)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20 + indcnt / 4, mem2->vs);
        if (!rc2)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20 + 2*indcnt / 4, mem2->vs);
        if (!rc3)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20 + 3*indcnt / 4, mem2->vs);
    }
    else
    {
        sceGumPushMatrix();    
        ScePspFVector3 tr  = { lx*DE2RA*R, ly*DE2RA*R, 0.0f };
        sceGumTranslate(&tr);
      
        if (!rc0)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20, mem2->vsf);
        if (!rc1)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20 + indcnt / 4, mem2->vsf);
        if (!rc2)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20 + 2*indcnt / 4, mem2->vsf);
        if (!rc3)
            sceGumDrawArray(GU_TRIANGLES, DRAW_OPTIONS, indcnt / 4, inds10x20 + 3*indcnt / 4, mem2->vsf);
    
        sceGumPopMatrix();
    }

    return true;
}

void Tile::qkey(char *b, char *chs) {
    int i, l = 0, mask;
    char d;

    int tj1 = (1 << tlev) - 1 - tj;

    for (i = tlev; i > 0; i--) {
        d = 0;
        mask = 1 << (i - 1);
        if (ti & mask)
            d++;
        if (tj1 & mask)
            d += 2;

        b[l++] = chs[d];
    }

    b[l] = 0;
}

void Tile::download() {
    switch (curlayer->urltype)
    {
        case URL_QKEY:
            {
                char qk[20];
                qkey(qk, "0123");
                sprintf(url, curlayer->url, snum, qk);
                if (++snum > 3) snum = 0;
                break;
            }
        case URL_YAHOO:
            sprintf(url, curlayer->url, ti, ((1 << (tlev - 1)) - 1 - ((1 << tlev) - 1 - tj)), 17 - tlev + 1);
            break;
        case URL_XYZ:
            sprintf(url, curlayer->url, snum, ti, (1 << tlev) - 1 - tj, tlev);
            if (++snum > 3) snum = 0;
            break;
        case URL_OSM:
            sprintf(url, curlayer->url, snum+'a', tlev, ti, (1 << tlev) - 1 - tj);
            if (++snum > 2) snum = 0;
            break;
            
        case URL_GMOON:
            sprintf(url, curlayer->url, tlev, ti, tj);
            break;
            
        case URL_GMARS:
            {
                char qk[20];
                qkey(qk, "qrts");
                sprintf(url, curlayer->url, qk);
                break;
            }
    }

    //if (dlcnt < 1)
    {
        dl = true;
        dlcnt++;
        dltile = this;
        //sceKernelSignalSema(dlsema, 1);
    }
}
