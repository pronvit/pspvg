TARGET			= PSPVirtualGlobe
OBJS			= CTimer.o main.o index.o memmgr.o tile.o vram.o menu.o vfpu.o jpeg/jdhuff.o jpeg/jdtrans.o jpeg/jerror.o jpeg/jdapimin.o jpeg/jdinput.o jpeg/jdatasrc.o jpeg/jdcolor.o jpeg/jcomapi.o jpeg/jutils.o jpeg/jdapistd.o jpeg/jdphuff.o jpeg/jdtrans.o jpeg/jdcoefct.o jpeg/jdmaster.o jpeg/jdsample.o jpeg/jdmainct.o jpeg/jddctmgr.o jpeg/jidctflt.o jpeg/jidctfst.o jpeg/jidctint.o jpeg/jdpostct.o jpeg/jdmarker.o jpeg/jmemmgr.o jpeg/jmemnobs.o jpeg/jdatamemsrc.o png/pngrio.o png/pngerror.o png/png.o png/pngmem.o png/pngread.o png/pngrtran.o png/pngtrans.o png/pngrutil.o png/pngset.o png/pngget.o


BUILD_PRX=1
PSP_FW_VERSION=371
EXTRA_TARGETS	= EBOOT.PBP
PSP_EBOOT_TITLE	= Virtual Globe	
PSP_LARGE_MEMORY = 1
#USE_PSPSDK_LIBC = 1


INCDIR			= jpeg png
LIBDIR			=
LDFLAGS			=
LIBS			= -lpsputility -lintraFont -lpspgum_vfpu -lpspssl -lpsprtc -lstdc++ -lpspvfpu -lpspgu -lpsphttp -lpspwlan -lz -lm
CFLAGS			= -O2 -G0 -Wall
CXXFLAGS		= $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS			= $(CFLAGS)

PSPSDK			= $(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak


#./CTimer.o: ./CTimer.cpp 
#	$(CXX) $(CXXFLAGS) -c ./CTimer.cpp -o ./CTimer.o
#	
#
#./main.o: ./main.cpp 
#	$(CXX) $(CXXFLAGS) -c ./main.cpp -o ./main.o
	




