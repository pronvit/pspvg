#ifndef vram_h
#define vram_h

#ifdef __cplusplus
extern "C" {
#endif

void* getStaticVramBuffer(unsigned int width, unsigned int height, unsigned int psm);
void* getStaticVramTexture(unsigned int width, unsigned int height, unsigned int psm);

#ifdef __cplusplus
}
#endif

#endif
