#ifndef _H_INDEX
#define _H_INDEX

#include "tile.h"

#define INDEX_HDR_IND "PSPVGINDA"
#define INDEX_HDR_IMG "PSPVGIMGA"

void index_open();
void index_close();

void index_read(Tile *t);
void index_write(Tile *t);

#endif
